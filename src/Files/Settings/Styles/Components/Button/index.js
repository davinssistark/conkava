import styled from 'styled-components';
import BaselineArrowForward from "react-md-icon/dist/BaselineArrowForward";
import RoundPlayCircleFilledWhite from "react-md-icon/dist/RoundPlayCircleFilledWhite";


export const Button = styled.button`
  position: relative;
  padding: 0.8rem 1.2rem;
  margin: 0 0.5rem;

  border-radius: 3px;
  overflow: hidden;
  background: ${props => (!props.transparent ? "#1041FB" : "transparent")};
  color: ${props => (!props.transparent ? "white" : "#1041FB")};
  cursor: pointer;
  font-weight: 500;
  font-size: 15px;
  border: ${props => (!props.transparent ? "0" : `solid 1px #CBCBCB`)};


  ${props=>props.center && `

  		${props.theme.utils.centerContent()}
  		${props.theme.utils.rowContent()}
  		height: 40px;
  		  margin: 0 auto;

  	`}
`;

export const Button1 = styled.button `
	background: ${props => props.theme.secondaryBlue};
	width: 105px;
	height: 34px;
	border: none;
	border-radius: 3px;
	color: ${props => props.theme.main};
	font-size: 14px;
	cursor: pointer;
	font-weight: ${props => props.theme.fontWeight.regular};
	cursor: pointer;
	position: relative;
	${(props) => props.theme.media.tablet `
	`}
`

export const ArrowIcon = styled(BaselineArrowForward) `
	color: ${props => props.theme.secondaryBlue};
`

export const PlayIcon = styled(RoundPlayCircleFilledWhite) `
	color: ${props => props.theme.secondaryBlue};
	font-size: 20px;
`
