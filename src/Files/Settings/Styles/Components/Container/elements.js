import styled from 'styled-components';

const Container = styled.div `
	background: ${props => props.theme.main};
	display: flex;
	flex-direction: column;
	flex-wrap: no-wrap;
	width: 100%;
	overflow-y: auto;
	overflow-x: hidden;
	position: relative;
	
	${(props) => props.theme.media.tablet`
		box-sizing: border-box;
		padding-top: 70px;
		`}

	${(props) => props.theme.media.phone`
		padding-top: 75px;
		`}
`
export default Container;
