import React, { Component } from 'react';
import styled from 'styled-components';

const _ = require('lodash');

export const SelectStyles = styled.select`
    padding: 0 1rem;
    
    font-size: 18px;
    margin: 0.5rem auto;
    width: 100%;
    height: 50px;
    border-radius: 5px;
    position: relative;
    background: white;
    color: black;
    border: 1px solid #E5E5E5;
     &:focus {
    outline: none !important;
    border:1.5px solid ${props=>props.theme.colors.green};
    
    }
  
`;


const InputStyles = styled.input`
    padding:  1rem;
    font-size: 18px;
    width: 100%;
    height: 50px;
    border-radius: 5px;
    position: relative;
    background: white;
    color: #212121;
    border: 1px solid #E5E5E5;  
    &:focus {
    outline: none !important;
    border:1.5px solid ${props=>props.theme.colors.green};
    
    }
    margin: 0.5rem auto;
`;
const Body = styled.div`
  
    position: relative;
  width: 100%;
   margin: 0 auto;
`;
const Tag = styled.label`
    position: absolute;
    color:${props=>props.theme.colors.green};
    background: white;
    padding:0 0.1rem;
    border-radius:2px;
    font-size: 12px;
    font-weight: 500;
    top: -1px;
    left: 0.7rem;
`;
export class Input extends Component{
  constructor(){
    super();
    this.state = {
      focus: false,
      value: ""
    }
  }
  
  
  render(){
      
    return (
    <Body>
        <InputStyles 
            onBlur = {()=>{this.setState({focus: false})}}
            onFocus = {()=>{
            this.setState({focus: true});
        }}
        type={this.props.type || "text" }
        maxlength={this.props.maxlength}
        onChange1={(e)=>{this.setState({value:e.target.value})}}
        onChange={(e)=>{this.props.onChange(e.target.value)}}
        value = {this.props.value} placeholder = {this.props.placeholder}/> 
    {(this.state.focus || this.props.value) && <Tag>{this.props.placeholder}</Tag>}
    
    </Body>)
      
  }
    
}
 

export const Select = (props)=>(

<Body>
    <SelectStyles
    value={props.value}
    onChange={(e)=>{props.onChange(e.target.value)}}
   > 
       {props.options && props.options.map((item, id)=>(<option 
       key={id}
       value={item.value}>{item.title}</option>))}
    </SelectStyles> 
    {/*props.value && <Tag>{props.placeholder}</Tag>*/}
</Body>)