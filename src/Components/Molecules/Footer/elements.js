import styled from 'styled-components';

export const Content = styled.div `
	display: flex;
	justify-content: space-between;
	margin-top: 5%;
	height: 90%;
	width: 77%; 
`;

export const Bottom = styled.div `
	height: 10%;
	width: 45%;
	margin-left: 11.2%;
	align-self: flex-start;
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: space-between;
	align-items: center;
	margin-bottom: 1%;
	div:nth-child(2) {
		color: ${props => props.theme.darkGray};
	}

	a {
		text-decoration: none;
	}
`;

export const Options = styled.div`
	height: 40%;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
`;

export const Dohnny = styled.div `
	width: 50%;
	${Options} {
		height: 45%;
	}
`;

export const Community = styled.div `
	width: 50%;
	${Options} {
		height: 60%;
	}
`;

export const About = styled.div `
	width: 50%;
	${Options} {
		height: 50%;
	}
`;

export const Info = styled.div `
	width: 35%;
	display: flex;
	flex-wrap: wrap;

	a {
		text-decoration: none;
	}
`;

export const Text = styled.div `
	color: ${props => props.theme.lightGray};
	font-weight: ${props => props.theme.fontWeight.regular}
	font-size: 14px;

	&:hover {
		text-decoration: underline;
	}
`;

export const NewsContent = styled.div `
	width: 80%;
	div:nth-child(1) {
		width: 85%;
	}

	${Text}:hover {
		text-decoration: none;
	}
`

export const Newsletter = styled.div `
	width: 45%;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	div: nth-child(1) {
		height: 10%;
	}
`;


export const Icons = styled.div `
	margin-top: 7%;
	display: flex;
	justify-content: space-between;
	width: 78%;
`;

export const Icon = styled.img `
	width: 36px;
	height: 36px;
`;

export const Title = styled.div `
	color: ${props => props.theme.darkGray};
	font-weight: ${props => props.theme.fontWeight.medium}
	font-size: 16px;
	height: 20%;
`;

export const Container = styled.div `
	background: ${props => props.theme.footerGray};
	width: 100%;
	height: 550px;
	display: flex;
	flex-direction: column;
	flex-wrap: no-wrap;
	align-items: center;
	justify-content: center;
	box-sizing: border-box;

	${(props) => props.theme.media.tablet `
		padding: 5%;
		height: 700px;
		${Content} {
			width: 100%;
			flex-direction: column;
			${Info} {
				width: 100%;
				height: 50%;
			}
			${Newsletter} {
				width: 100%;
				height: 50%;
				${Title}, ${NewsContent} {
					padding: 0 5%;
					width: 100%;
				}

				${Icons} {
					width: 100%;
				}
			}
		}
		${Bottom} {
			width: 100%;
			margin-left: 0;
		}
	`}
`;
