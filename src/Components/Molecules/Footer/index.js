import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const Title = styled.div`
  font-size: 30px;
  color: black;
  margin: 0 auto;
  text-align: left;
  font-weight: 800;
`;


export const Nav = styled.nav`
background:#bf9b30;
height: 45px;
width: 100%;
`;




export const Body = styled.div`
  width: 100%;
  margin: 0 auto;
  text-align: left;
  background: #f6f6f6;
  position: relative;

`;

export const Content = styled.div`
	width: 100%;
	${props=>props.theme.utils.centerContent()}
`;

export const Ul = styled.ul`
  color: black;
  font-size: 22px;
  width: 100%; 
  margin: 100px auto;

  text-align: left;
  li{ 
    margin: 1.5rem auto;
    list-style: none;
    color:black;
  }
  a{
  	text-decoration: none;
  	 color:black;
  	 opacity:0.7;
  }
    ${props=>props.theme.media.tablet`
      width: 100%;
      font-size: 16px;
     
    `}
`;

const Presentation = ({color, title, subtitle, img, features=[]}) => (
  
  <Body>

    <Content>
	
		<Ul>
			<li>
				<Title>
				Contacto:
				</Title>
			</li>

			<li>
				<b>Tel/Whatsapp:</b>
        <br/>
				<br/> 01 33-1107-2378 | 01 81-1605-4541
			</li>

			<li>
			<b>E-mail: </b>
			<br/>
         <br/>
			admin@conkava.com
			</li>

      <li>
      <b>Dirección: </b>
      <br/>
         <br/>
         <a target="_black" href="https://www.google.com/maps/place/9+85,+Seattle,+45150+Zapopan,+Jal./@20.7198123,-103.3738914,17z/data=!3m1!4b1!4m5!3m4!1s0x8428afcfc9e4e213:0x4d0c0e733a06dfab!8m2!3d20.7198123!4d-103.3717027">
          Calle 9 #85 Col. Seattle, Zapopan 45150.
          </a>
      </li>
      <li>
      <b>Social Media: </b>
      <br/>
         <br/>
         <a target="_black" href="https://www.facebook.com/ArquitecturaConkava/ ">
      <img width="50px" src={ require("static/imgs/fb.png")}/></a>
      </li>




     
		</Ul>
  </Content>
<Nav/>
	
  </Body>
);
export default Presentation;