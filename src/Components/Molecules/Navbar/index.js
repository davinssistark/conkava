import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import { Button } from 'Files/Settings/Styles/Components/Button';
import HelpIcon from "react-md-icon/dist/RoundHelpOutline";
import RoundMenu from "react-md-icon/dist/RoundMenu";
import RoundClose from "react-md-icon/dist/RoundClose";
import {Link, withRouter} from 'react-router-dom';

const _ = require('lodash');

 const LogoTitle = styled.div`
    font-size: 25px;
`;

 const Logo = styled.img`
    padding-top:2px;
    width: 23px;
    margin-left: -10px;
`;

 const Nav = styled.nav`
    top:0;
    left:0;
    position: fixed;
    overflow: hidden;
    width: 100%;

    background: ${props=>!props.nav?"transparent":"#212121"};
    transition: 0.3s;
    ${props=>props.nav && "box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);"}

    a{
      text-decoration: none;
      color: white;
      font-weight: 500;
    }

    &:hover
     { 


    a{
          color: ${props=>props.theme.color.navbarText};
    }


      background: white;
        ${props=>!props.noShadow && "box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);"}
     }


    height: 65px;
    ${props=>props.theme.utils.rowContent}
      z-index:9999999999999999999999999999999;
  
`;

 const NavContent = styled.nav`
    ${props=>props.theme.utils.rowContent}
    width: 90%;
    margin: 0 auto;
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;
    white-space:nowrap;
`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;

 const AlignEnd = styled(_Section)`
    justify-content: flex-end;
    order: 1;
`;

 const Separator = styled.div`
  color: #757575;
  position: relative;
  border-left: 1px solid #757575;
  width: 2px;
  height: 1.2rem;
  opacity: 0.5;
`;

const Icon = styled.i`
  vertical-align: bottom;
`;


const NavsContainer = styled.div`
  z-index:2;

  ${props=> props.shadow && ` 
    box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);
    position: fixed;
    top:0;
    width: 100%;
    height: ${65*2}px;
  `}
`;

const TabSet = styled.div`
  border: 1px red solid;
  display: ${props=>props.notNavbar?"none": "inline"};
  @media (max-width: 700px) {
    display: none;
  }
`;

 const Item = styled.div`
    position: relative; 
    height: 65px;
    cursor: pointer;
    min-width: 45px;
    font-size: 15px;
    font-weight: 500;
    
    ${props=>props.theme.utils.centerContent()}
    ${props=>props.cursor && "cursor: pointer;"}
    ${props=>props.margin && "margin: 0 0.5rem;"}
    ${props=>props.hideBig && `
        display: none;
        @media (max-width: 700px)
        {
            ${props.theme.utils.centerContent()}
        }
    `}

    ${props=>props.hide && `
        @media (max-width: 700px)
        {
          display: none;
        }
    `} 

    
`;

const TabItem = ({children, active,hide ,onClick})=>{

  const TabSelector = styled.div`
    background: ${(props)=>props.theme.colors.green};
    position: absolute;
    width: 65%;
    height: 4px;
    font-size: 15px;
    border-radius: 5px 5px 0 0;
    bottom: 0;
    left:17.5%;
  `;

  return(
    <Item 
    hide
      style={{margin:"0 0.8rem"}}
      onClick={onClick} 
      active={active}
      cursorPointer>
      {children}
      {active &&  <TabSelector/>}
    </Item>
    )
}


 const ButtonMore = styled.button`
  background: white;
  color: ${props=>props.theme.colors.green};
  border : none;
  box-shadow: 0 0.5px 5px rgba(0,0,0,0.2);
  width: 120px;
  position: relative;
  padding: 0.6rem 0.8rem;
  margin: 0 0.5rem;
  border-radius: 20px;
  overflow: hidden;
  cursor: pointer;
  font-size: 16px;
`;

const logo = require("static/imgs/logo.png")

class Header extends Component
{
  constructor(props) 
  {
      super(props)
      this.state = {
        sidebar: false,
        nav: false,
      }
  }


 componentDidMount() {
  window.addEventListener("scroll", this.handleScroll);
  }

  handleScroll = e => {
  const scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
  
  if(scrollTop > 50)
  { 
    if(!this.state.nav)
       this.setState({nav:true})
  }
  else
   {
    if(this.state.nav)
    this.setState({nav:false})}
  };

  
  render() {
    const tab = this.props.location.pathname
    return (  
    <React.Fragment>  
    <NavsContainer
    shadow= {this.state.sidebar}

    >
          <Nav
            nav={this.state.nav} 
            noShadow={this.state.sidebar}
           >
          <NavContent>
          <AlignStart>

          <Item style={{background: "white",   marginRight:"2rem",}}>
              
             <img src={logo} width="62px"/>
            </Item>
      
            <Item 
            hide
              style={{
                marginRight:"1rem",
                marginLeft: "-0.3rem" }}>
              
              <Link to="/"
                style={{ 
                fontSize: "22px",
                fontWeight:"300"
               
                }}>
                  Conkava<b> Arquitectos</b>
              </Link>
            </Item>
            </AlignStart>

            <AlignEnd> 
            <TabItem 
              margin 
              hide
              active = {tab === ("/docs")}
              style={{ 
              fontSize:"16px", 
              }}>

            <a href="#servicios">
            Servicios
            </a>

            </TabItem>

                    <TabItem 
                    hide
                      margin
                      active = {tab === ("/examples")}
                      style={{ 
                        fontSize:"16px", 
                      }}>



                    <a 
                    href="#portafolio">
                    Portafolio 
                    </a>

                    </TabItem>

       

                  <TabItem
                    active = {
                      tab === ("/about") ||
                      tab === ("/about/") ||
                      tab === ("/about/team") ||
                      tab === ("/about/product") ||
                      tab === ("/about")
                    } 
                    margin 
                    style={{ 
                      fontSize:"16px",  
                    }}>

                    <a href="#modelado">
                    Modelos en Software
                    </a>

                  </TabItem>

               <TabItem
                    active = {
                      tab === ("/about") ||
                      tab === ("/about/") ||
                      tab === ("/about/team") ||
                      tab === ("/about/product") ||
                      tab === ("/about")
                    } 
                    margin 
                    style={{ 
                      fontSize:"16px",  
                    }}>

                    <a href="#contancto">
                    Contacto
                    </a>

                  </TabItem>

            </AlignEnd>
            </NavContent>
            </Nav> 



          </NavsContainer>

          </React.Fragment>
      );
  }
}

export default withRouter(Header)

