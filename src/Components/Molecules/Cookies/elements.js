import styled from 'styled-components';


export const Container = styled.div `
	display: flex;
	width: 100%;
	height: 60px;
	position: fixed;
	bottom: 0;
	background: ${props => props.theme.colors.green};
	z-index: 999999999999;
	box-sizing: border-box;
	align-items: center;
	justify-content: space-between;
	padding: 0 5%;
`

export const Text = styled.div `
	
	font-size: 14px;
	color: white;

	${props => props.theme.media.tablet `
		font-size: 9px;
	`}
`


export const Button = styled.button `
	width: 100px;
	height: 34px;
	background: white;
	color: ${props => props.theme.secondaryBlue};
	padding: 0 10px;
	display: flex;
	align-items: center;
	justify-content: center;
	border: none;
	border-radius: 3px;
	cursor: pointer;
	${props => props.theme.media.tablet `
		font-size: 9px;
	`}
`