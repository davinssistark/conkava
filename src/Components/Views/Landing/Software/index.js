import React, {useState, useEffect}from 'react';
import styled from 'styled-components';


export const Title = styled.div`
  font-size: 20px;
  color: #202124;
  margin: 0 auto;
  margin-top: 1.3rem;
  text-align: center;
  width: 80%;
  ${props=>props.theme.media.tablet`


       font-size: 16px;
    `}

`;

export const Dot = styled.div`
 width: 12px;
 height: 12px;
 background: ${props=>props.active?"orange":"white"};
 border-radius: 50%;

 ${props=>props.theme.media.tablet`

     width: 20px;
     height: 20px;
    `}

`;


export const DotContainer = styled.div`
 width: 30%;
 padding: 1rem 2rem;
 background: rgba(0,0,0,0.4);
 border-radius: 17px;
position: absolute;
bottom: 2rem;
left:35%;
${props=>props.theme.utils.rowContent()}

 ${props=>props.theme.media.tablet`
      left:20%;

      width: 60%;
    `}



`;




export const Head = styled.div`
  font-size: 60px;
  color: #202124;
  width: 50%;
  margin: 2.5rem auto;
  text-align: center;
  padding-top: 3rem;
    ${props=>props.theme.media.tablet`

      width: 100%;
       font-size: 30px;
    `}
`;

export const Subtitle = styled.div`
  font-size: 17px; 
  color: #5F6368;
  text-align: left;
  margin: 1rem auto;
`;

export const Ul = styled.ul`
  color: #5F6368;
  font-size: 20px;
  display: inline-block;
  margin: 0 auto;
  text-align: left;
  li{ 
    margin: 1.5rem auto;
  }
`;

export const Body = styled.div`
  width: 100%;
  position: relative;
  margin: 0 auto;
  text-align: center;
  background: #212121;
  color: white;

  
`;

export const Content = styled.div`
  width: 92%;
  margin: 0 auto;
  text-align: center;
  padding: 4rem 0;
  ${props=>props.theme.utils.rowContent()}
    ${props=>props.theme.media.tablet`
      width: 100%;
       display: block;
       padding: 1rem 0;
    `}
`;

export const Card = styled.div`
  position: relative;
  width: ${60}%;
  margin: 1rem auto;
  text-align: center;
  height: auto;
  background: #212121;
  border-radius: 10px;
  overflow: hidden;
  padding: 0;
  box-shadow: 0 0.5px 5px rgba(0,0,0,0.5);
  &:hover{
    
  }
  a{
    color: #1041FB;
    text-transform: uppercase;
    font-size: 16px;
    text-decoration: none;
    position: absolute;
    bottom: 2.2rem;
    left: 1.5rem;
  }

    ${props=>props.theme.media.tablet`

      width: 90%;
      margin-top: 0.2rem;
      height: auto;
       
    `}
`;

export const ImgContainer = styled.div`
  width: 100%;
  margin: 0 auto;
`;

export const Img = styled.img`
  width: 100%;
`;

export const FooterImg = styled.img`
  width: 45%;
  margin: 4rem auto;
`;


export const H1 = styled.h1`
  width: 45%;
  margin: 4rem auto;
   ${props=>props.theme.media.tablet`

      width: 90%;
   
       
    `}
`;

export const P = styled.p`
  width: 35%;
 margin:2rem auto; 
 font-size:18px;

   ${props=>props.theme.media.tablet`
      font-size:16px;
      width: 90%;
   
       
    `}
`;



const Imgs = [require("static/imgs/aa4.png"), require("static/imgs/aa5.png"), require("static/imgs/aa6.png")]

const Presentation = ({color, title, subtitle, img, features=[]}) =>{ 

  
    const [count, setCount] = useState(0);

  return (
  <Body>

    <Content>
    <div style={{width: "100%", paddingTop:"30px"}}>
  <H1 >Modelado en software</H1>
    <P>

Servicios de Renderizado con software para proyectos con: 3d max, Revit, Autocad 2d y 3d Avanzado, Photoshop, sketch up con V-ray, Lumion, Corel Draw

  </P>
 <br/>
<br/>
 </div>
    <Card>
      <ImgContainer>
     <Img 
     
     src={Imgs[count]}/>
     <DotContainer>
     <Dot active={count===0} onClick={()=>{setCount(0)}}/>
     <Dot active={count===1} onClick={()=>{setCount(1)}}/>
     <Dot active={count===2} onClick={()=>{setCount(2)}} />

     </DotContainer>
      </ImgContainer>
     

 
      </Card> 

        <div>



  </div>
 
     



  </Content>
  </Body>
)}

export default Presentation;
