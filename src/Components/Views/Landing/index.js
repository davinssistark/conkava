import React from 'react';
import Navbar from "Components/Molecules/Navbar"
import Footer from "Components/Molecules/Footer"
import Value from "./Value"
import Header from "./Header"
import Portafolio from "./Portafolio"
import Software from "./Software"

const Landing = () => (

  <React.Fragment>
    <Navbar /> 
      <div style={{zIndex:"0", "width":"100%", overflow:"hidden"}}>
        <Header/> 
        <div id="servicios"/>  
        <Value/>
        <div id="portafolio"/>   
        <Portafolio/>
        <div id="modelado"/>  
        <Software />
        <div id="contancto"/>  
        <Footer />
      </div>
  </React.Fragment>
)

export default Landing;
