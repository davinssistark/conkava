import React from 'react';
import styled from 'styled-components';



export const Title = styled.div`
  font-size: 30px;
  font-weight: 500;
  color: #202124;
  margin: 0 auto;
  margin-top: 1.3rem;
  text-align: center;
  width: 80%;

   ${props=>props.theme.media.tablet`
      width: 100%;
        font-size: 20px;
       
    `}
`;

export const Subtitle = styled.div`
  font-size: 17px; 
  color: #5F6368;
  text-align: left;
  margin: 1rem auto;
`;

export const Card = styled.div`
  position: relative;
  width: 49%;
  margin: 3rem auto;
  text-align: center;
  height: 550px;
  background: transparent;
  border-radius: 10px;
  padding: 2.2rem 1.5rem;
  ${props=>props.theme.media.tablet`
    width: 100%;
    margin-top: 1.5rem;
      height: auto;
     
  `}
`;

export const ImgContainer = styled.div`
  width: 80%;
  margin: 0 auto;
`;

export const Img = styled.img`
  width: 100%;
`;

const CardPresentation = ({title, img}) => (
  <Card>
    <ImgContainer>
    <Img src={img}/>
    </ImgContainer>
    <Title>
    {title}
    </Title>
  </Card>  
);

export default CardPresentation;