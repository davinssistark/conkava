import React from 'react';
import styled from 'styled-components';
import Card from "./Card"
import { Button, PlayIcon } from 'Files/Settings/Styles/Components/Button';
import {Link} from "react-router-dom"
export const Title = styled.div`
 font-size: 60px;
 color: #202124;
 font-weight: 500;
  width: 70%;
  margin: 100px auto;
  
       text-align: center;
     ${props=>props.theme.media.tablet`

      width: 100%;
       font-size: 30px;
    `}
`

export const Heading = styled.div`
 font-size: 40px;
 color: #202124;
 font-weight: 500;
  width: 70%;
  margin: 50px auto;
  
       text-align: center;
     ${props=>props.theme.media.tablet`

      width: 100%;
       font-size: 25px;
    `}
`
export const Subtitle = styled.div`
 font-size: 20px; 
 color: #65696B;
  width: 70%;
  text-align: center;
  margin: 2rem auto;
  line-height: 35px;
  font-weight: 400;
  ${props=>props.theme.media.tablet`
font-size: 16px;
 line-height: 26px;
      width: 100%;
       
    `}
`

export const Row = styled.div`

  
 ${props =>props.theme.utils.rowContent()}
  

`



export const Body = styled.div`

      width: 100%;
      position: relative;
      margin: 0 auto;
      text-align: center;
      

    ${props =>props.background === "black" && ` 
        background: #1D1D1D;
        ${Title},${Subtitle}{
          color: white;
          li{
            color: #EBEBEB;
          }
        }
        ${Subtitle}{
          opacity:0.77;
        }

    `}
   ${props =>props.background === "gray" && ` 
        background: #F5F5F5;
        ${Title},${Subtitle}{
          color: #202124;
        }
        ${Subtitle}{
          opacity:0.77;
        }
    `}
`

export const Content = styled.div`
 width: 93%;
  margin: 0 auto;
  text-align: center;
  padding: 4rem 0;

   ${props=>props.theme.media.tablet`

      width: 80%;
       
    `}
`

export const ImgContainer = styled.div`
border-radius: 4px;
width: 100%;
`

export const Img = styled.img`
  width: 100%;
`

export const FooterImg = styled.img`
   width: 45%;
   margin: 4rem auto;
`


const Presentation = ({color, title, heading, subtitle, img, features=[]}) => (
  
  <Body background={color}>
  
  <Content>
 
    <Title>
      {title}
    </Title>

      <Button center transparent style={{border:"none"}}><PlayIcon style={{marginRight:"1rem"}}/> Watch video</Button>
<br/>
<br/>



    <ImgContainer>
     <Img src={img}/>
    </ImgContainer>
    <Heading>{heading}</Heading>

     <Subtitle>
      {subtitle}
    </Subtitle>

    <br/>
    <Link to="/docs">
    <Button transparent style={{border:"none"}}>Learn more</Button>
</Link>
    <Row>
    {features.map((item)=><Card  {...item}/>)}
   
 
    </Row>

       <br/>

    <Link to="/login"><Button>Early access</Button></Link>
    <br/>
    <br/>
    <Link to="/examples"><Button transparent style={{border:"none"}}>View Dohnny examples</Button></Link>

  </Content>
  </Body>
)

export default Presentation;
