import React, {useState} from 'react';
import styled from 'styled-components';
import {Container, Img} from 'local_npm/react-container';


export const Title = styled.div`
  font-size: 20px;
  color: #202124;
  margin: 0 auto;
  margin-top: 3rem;
  text-align: center;
  width: 80%;
  ${props=>props.theme.media.tablet`


       font-size: 16px;
    `}

`;

export const Head = styled.div`
  font-size: 60px;
  color: #202124;
  width: 50%;
  margin: 2.5rem auto;
  text-align: center;
  padding-top: 3rem;
    ${props=>props.theme.media.tablet`

      width: 100%;
       font-size: 30px;
    `}
`;

export const Subtitle = styled.div`
  font-size: 17px; 
  color: #5F6368;
  text-align: left;
  margin: 1rem auto;
`;

export const Ul = styled.ul`
  color: #5F6368;
  font-size: 20px;
  display: inline-block;
  margin: 0 auto;
  text-align: left;
  li{ 
    margin: 1.5rem auto;
  }
`;

export const Body = styled.div`
  width: 100%;
  position: relative;
  margin: 0 auto;
  text-align: center;
  background: #f6f6f6;
  color: black;
 
   ${props=>props.theme.utils.rowContent()}
    ${props=>props.theme.media.tablet`
      margin-top: 10px;
    `}
`;

export const Content = styled.div`
  width: 90%;
  margin: 0 auto;
  text-align: center;
  padding: 4rem 0;
  ${props=>props.theme.utils.rowContent()}
    ${props=>props.theme.media.tablet`
      width: 100%;
       display: block;
       padding: 1rem 0;
    `}
`;

export const Card = styled.div`
  position: relative;
  width: 48%;
  margin: 1rem auto;
  text-align: center;
  height: 600px;
  background: white;
  border-radius: 5px;
  overflow: hidden;

  box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);
   a{
    color: #1041FB;
    text-transform: uppercase;
    font-size: 16px;
    text-decoration: none;
    position: absolute;
    bottom: 2.2rem;
    left: 1.5rem;
  }

    ${props=>props.theme.media.tablet`

      width: 90%;
      margin-top: 0.2rem;
      height: auto;
       
    `}
`;



export const Center = styled.div`

   ${props=>props.theme.utils.centerContent()}
   height:250px;
   padding: 0 2rem;

`;

export const Label = styled.label`

   ${props=>props.theme.utils.centerContent()}
   padding: 1.2rem 0.9rem;
   border-radius: 17px;
   min-width: 150px;
   font-size: 12px;
   height: 30px;
   color: orange;
   background: white;
   border: orange 1px solid ;
   overflow: hidden;


`;


export const MoreBTN = styled.button`

   ${props=>props.theme.utils.centerContent()}
   padding: 1.2rem 0.9rem;
   border-radius: 5px;
   width: 150px;
   height: 30px;
   color: #212121;
 
   overflow: hidden;
   border: none;
   margin: 2rem auto;
   cursor: pointer;

`;


export const H1 = styled.h1`
  width: 45%;
  margin: 4rem auto;
   ${props=>props.theme.media.tablet`

      width: 90%;
   
       
    `}
`;

export const P = styled.p`
  width: 45%;
 margin:2rem auto; 
 font-size:22px;

   ${props=>props.theme.media.tablet`
      font-size:16px;
      width: 90%;
   
       
    `}
`;


const DATA = [ {

  date:"AÑO 2016 A 2017",
  description:`
  CASA ALTAVISTA MONTERREY. Diseño de fachadas y renderizado, ejecución de plomerías hidráulicas, sanitarias y pluviales. Construcción de alberca y terraza.

  `,
  type:"DISEÑO Y CONSTRUCCIÓN",
  img:require("static/imgs/aa0.png"),
},
{

  date:"AÑO  2017",
  description:`
  CASA EN PLAYA PUERTO MELAQUE. Diseño de proyecto de remodelación y ejecución de obra, intervención de un 80% de la construcción antigua. Renderizado.
  `,
  type:"REMODELACIÓN",
  img:require("static/imgs/aa1.png"),
},
{

  date:"MONTERREY AÑO 2018",
  description:`
  REMODELACIÓN CASA LUNAS PARA LA FAMILIA VELAZQUEZ, SAN PEDRO GARZA GARCÍA.

  `,
  type:"REMODELACIÓN",
  img:require("static/imgs/aa2.png"),
},
{

  date:"AÑO 2019",
  description:`
  REMODELACIÓN OFICINAS CORPORATIVAS CLIENTE ONE IT. Renderizado y Diseño
  `,
  type:"REMODELACIÓN",
  img:require("static/imgs/aa3.png"),
}



]


const Presentation = ({color, title, subtitle, img, features=[]}) => {

      const [flag, setFlag] = useState(false);


  return(
  
  <Body>
<br/>
<br/>
  <H1 style={{width: "45%", margin:"4rem auto"}}>Portafolio</H1>
 
  <br/>

    <Content>
    <Container row>
{DATA.map((data, id)=>(
<Card key={id}>
<Container backgroundImg={data.img} height="320px" width="100%" overflowHidden/>
      <Label style={{position:"absolute", left:"1rem", top:"1rem", background:"white", color:"rgba(0,0,0,0.9)", border:"none", fontSize:"18px", fontWeight:"800"}}>
      {data.date}
      </Label>




      <Center style={{width:"90%", margin:"0 auto", fontWeight:"300", fontSize:"16px"}}>
      <h4 style={{fontWeight:"800", fontSize:"22px"}}>{data.type}</h4>
       {data.description}
      </Center>


    </Card> 

  ))}
    
</Container>
  </Content>
  </Body>
)}

export default Presentation;
