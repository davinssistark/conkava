import React from 'react';
import styled, {keyframes} from 'styled-components';

const scale = keyframes`
  0% {
    transform: scale(1);
  }

  50% {
    transform: scale(1.5);
  }

  0% {
    transform: scale(1);
  }
`;
export const Body = styled.div`
  width: 100%;
  margin: 0 auto;
  text-align: center;
  overflow: hidden;
   ${props=>props.theme.media.tablet`
     height: 100%;
      
    `}
`


export const Container = styled.div`
  width: 65%;
    ${props=>props.theme.media.tablet`
     width: 90%;
      
    `}
 
`
export const ColorWindow = styled.div`
  position: absolute;
  top: 0;
  left:0;
  width: 100%;
  height: 100vh;
  background: rgba(0,0,0,0.5);
  z-index: 2;
`
export const Title = styled.div`
  font-size: 70px;
  color: white;
  font-weight: 500;
  width: 90%;
  margin: 0 auto;
  margin-top: 140px;
  text-align: left;
  ${props=>props.theme.media.tablet`
     margin-top: 80px;
    width: 90%;
    font-size: 40px;
  `}
`;

export const Subtitle = styled.div`
 font-size: 25px; 
 color: white;
  width: 90%;
  text-align: left;
  margin: 0.2rem auto;
  line-height: 35px;
  font-weight: 400;
  ${props=>props.theme.media.tablet`
      font-size: 16px;
      width: 90%;
      line-height: 26px;
       margin-bottom: 90px;
    `}
`;

export const ImgContainer = styled.div`
margin: 0 auto;
position: fixed;
top:0;
left: 0;
width: 100%;
z-index: -1;
height: 100vh:
 
`
export const Img = styled.img`
  width: 100%;
  z-index: 0;
  animation: ${scale} 60s linear infinite;
   ${props=>props.theme.media.tablet`
    height: 100vh;
    width: auto;
    `}
`

const img1 = require("static/imgs/land1.jpeg");
//const img2 = require("static/imgs/land2.png");

export const Button = styled.button`
cursor: pointer;
border: 1px solid rgba(255,255,255,0.8); 
border-radius: 5px;
background: rgba(255,255,255,0.8);
padding: 1rem 1.5rem;
color: black;
font-size:20px;
margin-top: 20px;
&:hover{ 
  background: rgba(255,255,255,0.5);
  color: white;
  border-color:white;
}
`
const Landing = () => (
   <React.Fragment>
  <Body>
    <Container>
      <Title>
     Hacer arquitectura con calidad humana es nuestra pasión.
      </Title>
      <Subtitle>
      Construir y diseñar con la meta de servir a los demás y ofrecer espacios dignos.
        <br/>
       <a href="#contancto">
              <Button>Contáctanos</Button>
              </a>
      </Subtitle>

    </Container>

    <ImgContainer>
      <Img src={img1}/>
      <ColorWindow />
    </ImgContainer>
  </Body>
  </React.Fragment>
)

export default Landing;
