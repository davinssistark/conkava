import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import BaselineSettingsApplications from "react-md-icon/dist/BaselineSettingsApplications";
import BaselineBusiness from "react-md-icon/dist/BaselineBusiness";
import BaselineDescription  from "react-md-icon/dist/BaselineDescription";
import BaselineEdit  from "react-md-icon/dist/BaselineEdit";

const Landing = () => (
   <React.Fragment>
   <Container overflowHidden>
    <Container row width="90%" margin="150px auto" css="margin-top:110px; ">
     <Container 
       border="1px solid white"
        width="23%" 
        height="auto" 
        overflowHidden
        tabletCSS="width:100%; margin-top:1rem;"
        
        css="padding:2rem; border-radius:5px; text-align: left;">
        <BaselineSettingsApplications style={{fontSize:"40px",marginLeft:"40%" }}/><br/><br/>
        Mantenimiento en albañilerías, plomerías, acabados en muros, pisos, plafonería y cualquier imperfecto en construcciones ya sea de carácter habitacional, comercial u oficinas, industrial o naves industriales, etc.
        </Container>

         <Container

        border="1px solid white"
        width="23%" 
        height="auto" 
        overflowHidden
        tabletCSS="width:100%; margin-top:1rem;"
        
        css="padding:2rem; border-radius:5px; text-align: left;">
<BaselineBusiness style={{fontSize:"40px",marginLeft:"40%" }}/><br/><br/>

       Diseño arquitectónico y ejecución de obra, así como gestión de proyectos ante ayuntamiento etc.

        </Container>

         <Container 
       border="1px solid white"
        width="23%" 
        height="auto" 
        overflowHidden
        tabletCSS="width:100%; margin-top:1rem;"
        
        css="padding:2rem; border-radius:5px; text-align: left;">

<BaselineEdit style={{fontSize:"40px", marginLeft:"40%" }}/><br/><br/>
       Diseño y cálculo de ingenierías hidrosanitarias tales como electricidad para proyectos habitacionales, además ingeniería en Hidráulico, Sanitario, Condensados, Pluviales y Riego para proyectos tipo habitacional, verticales, comercial e industrial. 
        </Container>

         <Container 
       border="1px solid white"
        width="23%" 
        height="auto" 
        overflowHidden
        tabletCSS="width:100%; margin-top:1rem;"
        
        css="padding:2rem; border-radius:5px; text-align: left;">
    <BaselineDescription style={{fontSize:"40px",marginLeft:"40%" }}/><br/><br/>
Contamos con la Certificación de la empresa suiza Geberit, en la disciplina de drenaje sanitario HDPE, así como, drenaje pluvial para azoteas en verticales mediante sistema Geberit Pluvia.

        </Container>
     
    </Container>
    </Container>

    </React.Fragment>
);

export default Landing;