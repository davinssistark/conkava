import React from 'react';
import styled from 'styled-components';
import Items from "./Items"
import {Container } from 'local_npm/react-container';
export const Title = styled.div`
  font-size: 20px;
  color: #202124;
  margin: 0 auto;
  margin-top: 1.3rem;
  text-align: center;
  width: 80%;
  ${props=>props.theme.media.tablet`


       font-size: 16px;
    `}

`;

export const Head = styled.div`
  font-size: 60px;
  color: #202124;
  width: 50%;
  margin: 2.5rem auto;
  text-align: center;
  padding-top: 3rem;
    ${props=>props.theme.media.tablet`

      width: 100%;
       font-size: 30px;
    `}
`;

export const Subtitle = styled.div`
  font-size: 17px; 
  color: #5F6368;
  text-align: left;
  margin: 1rem auto;
`;

export const Ul = styled.ul`
  color: #5F6368;
  font-size: 20px;
  display: inline-block;
  margin: 0 auto;
  text-align: left;
  li{ 
    margin: 1.5rem auto;
  }
`;

export const Body = styled.div`
  width: 100%;
  position: relative;
  margin: 0 auto;
  text-align: center;
  background: #212121;
  color: white;
  margin-top: 110px;
  padding-bottom: 100px;
   ${props=>props.theme.utils.rowContent()}
    ${props=>props.theme.media.tablet`
      margin-top: 10px;
    `}
`;

export const Content = styled.div`
  width: 92%;
  margin: 0 auto;
  text-align: center;
  padding: 4rem 0;
  ${props=>props.theme.utils.rowContent()}
    ${props=>props.theme.media.tablet`
     
       //display: block;
       padding: 1rem 0;
    `}
`;

export const Card = styled.div`
  position: relative;
  width: ${100/4-2}%;
  margin: 1rem auto;
  text-align: center;
  height: 150px;
  background: #212121;
  border-radius: 10px;
  overflow: hidden;
  padding: 0;
  box-shadow: 0 0.5px 5px rgba(0,0,0,0.5);
  &:hover{
    
  }
  a{
    color: #1041FB;
    text-transform: uppercase;
    font-size: 16px;
    text-decoration: none;
    position: absolute;
    bottom: 2.2rem;
    left: 1.5rem;
  }

    ${props=>props.theme.media.tablet`
       height: 80px;
      width: 48%;
      margin-top: 0.2rem;
     // height: auto;
       
    `}
`;

export const ImgContainer = styled.div`
  width: 100%;
  margin: 0 auto;
`;

export const Img = styled.img`
  width: 100%;
  ${props=>props.theme.media.tablet`
      

     
       
    `}
`;

export const FooterImg = styled.img`
  width: 45%;
  margin: 4rem auto;
`;


export const H1 = styled.h1`
  width: 45%;
  margin: 4rem auto;
   ${props=>props.theme.media.tablet`

      width: 90%;
   
       
    `}
`;

export const P = styled.p`
  width: 45%;
 margin:2rem auto; 
 font-size:22px;

   ${props=>props.theme.media.tablet`
      font-size:16px;
      width: 90%;
   
       
    `}
`;

const Presentation = ({color, title, subtitle, img, features=[]}) => (
  
  <Body>
<br/>
<br/>
<br/>
  <H1 style={{width: "45%", margin:"4rem auto"}}>
  Servicios</H1>
 
  <br/>
  <div>

  <Items/>
 
  </div>

    <Content>

    <Card>
      <ImgContainer>
     <Img 
     style={{marginTop:"-150px"}}
     src={require("static/imgs/servic33.jpg")}/>
      </ImgContainer>
     
      </Card> 
 
      <Card> 

      <ImgContainer>
        <Img src={require("static/imgs/servic1.png")}/>
      </ImgContainer>
      
        

      </Card>

      <Card>
      <ImgContainer>
      <Img src={require("static/imgs/servic0.png")}/>
      </ImgContainer>
      
     
      
      </Card>

      <Card>
      <ImgContainer>
     <Img src={require("static/imgs/servic2.jpg")}/>
      </ImgContainer>
      
    
      
      </Card>  



  </Content>
  </Body>
)

export default Presentation;
