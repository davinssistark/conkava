import React from 'react'
import styled, { css } from 'styled-components';

const typography_font =  {
  external: "poppins",
  default: "roboto"
}

const typography_font_weight_values =  {
  thin: 100,
  light: 300,
  regular: 400,
  medium: 500,
  bold: 700,
  black: 900
}

const types = {
 "display4": `
    font-size: 7rem; /* 112sp */
    line-height: 7rem: /* 112sp */
    font-weight: ${typography_font_weight_values["light"]};
    letter-spacing: -.04em;
    margin: -1rem 0 3.5rem -.085em /* -16sp 0 56sp -.085em */;
    text-decoration: inherit;
    text-transform: inherit;
   `,
  "display3": `
    font-size: 3.5rem; /* 56px */
    line-height: 3.5rem; /* 56px */
    font-weight: ${typography_font_weight_values["regular"]};
    letter-spacing: -.02em;
    margin: -8px 0 64px -.07em;
    text-decoration: inherit;
    text-transform: inherit;
  `,
  "display2": `
    font-size: 2.813rem; /* 45px */
    line-height: 3rem; /* 48px */
    font-weight: ${typography_font_weight_values["regular"]};
    letter-spacing: normal;
    margin: -.5rem 0 4rem -.07em /* -8sp 0 64sp -.07em */;
    text-decoration: inherit;
    text-transform: inherit;
  `,
  "display1": `
    font-size: 2.125rem; /* 34sp */
    line-height: 2.5rem; /* 40sp */
    font-weight: ${typography_font_weight_values["regular"]};
    letter-spacing: normal;
    margin: -.5rem 0 4rem -.07em /* -8sp 0 64sp -.07em */;
    text-decoration: inherit;
    text-transform: inherit;
  `,
  "headline": `
    font-size: 1.5rem; /* 24sp */
    line-height: 2rem; /* 32sp */
    font-weight: ${typography_font_weight_values["regular"]};
    letter-spacing: normal;
    margin: -.5rem 0 1rem -.06em /* -8sp 0 16sp -.06em */;
    text-decoration: inherit;
    text-transform: inherit;
  `,
  "title": `
    font-size: 1.25rem; /* 20sp */
    line-height: 2rem; /* 32sp */
    font-weight: ${typography_font_weight_values["medium"]};
    letter-spacing: .02em;
    margin: -.5rem 0 1rem -.05em /* -8sp 0 16sp -.05em */;
    text-decoration: inherit;
    text-transform: inherit;
  `,
  "subheading2": `
    font-size: 1rem; /* 16sp */
    line-height: 1.75rem; /* 28sp */
    font-weight:${typography_font_weight_values["regular"]};
    letter-spacing: .04em;
    margin: -.5rem 0 1rem -.06em /* -8sp 0 16sp -.06em */;
    text-decoration: inherit;
    text-transform: inherit;
  `,
  "subheading1": `
    font-size: .938rem; /* 15sp */
    line-height: 1.5rem; /* 24sp */
    font-weight: ${typography_font_weight_values["regular"]};
    letter-spacing: .04em;
    margin: -.313rem 0 .813rem -.06em /* -5sp 0 13sp -.06em */;
    text-decoration: inherit;
    text-transform: inherit;
  `,
  "body2": `
    font-size: .875rem; /* 14sp */
    line-height: 1.5rem; /* 24sp */
    font-weight: ${typography_font_weight_values["medium"]};
    letter-spacing: .04em;
    margin: -.25rem 0 .75rem 0 /* -4sp 0 12sp 0 */;
    text-decoration: inherit;
    text-transform: inherit;
  `,
  "body1": `
    font-size: .875rem; /* 14sp */
    line-height: 1.25rem; /* 20sp */
    font-weight: ${typography_font_weight_values["regular"]};
    letter-spacing: .04em;
    margin: -.25rem 0 .75rem 0 /* -4sp 0 12sp 0 */;
    text-decoration: inherit;
    text-transform: inherit;
  `,
  "caption": `
    font-size: .75rem; /* 12sp */
    line-height: 1.25rem; /* 20sp */
    font-weight: ${typography_font_weight_values["regular"]};
    letter-spacing: .08em;
    margin: -.5rem 0 1rem -.04em /* -8sp 0 16sp -.04em */;
    text-decoration: inherit;
    text-transform: inherit;
  `,
  "button": `
    font-size: .875rem; /* 14sp */
    line-height: 2.25rem; /* 36sp */
    font-weight: ${typography_font_weight_values["medium"]};
    letter-spacing: .04em;
    margin: inherit /* We do not have adjust margin for button */;
    text-decoration: none;
    text-transform: uppercase;
  `
}


export function elevation(n = 1, color = ambient_color) {
  return `
    box-shadow: ${elevation_ambient_map[(n >= 0 && n <= 24)? n:0]} ${color};
  `;
}

const Text({children}) =>(<p>{children}</p>)

export Text;

