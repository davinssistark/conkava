import React, {Fragment} from 'react';
import Container from './elements';
//import Cookies from './../Cookies';

class App extends React.Component {
	render() {
		return(
			<Container>
				{this.props.children}
				
			</Container>
		)
	}
}

export default App;
