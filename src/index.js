import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import theme from './Components/Styles/theme';
import {ThemeProvider} from 'styled-components';
import { BrowserRouter } from 'react-router-dom';




const Init = ()=>(
	<ThemeProvider theme={theme}>
		<BrowserRouter>
			<App />
		</BrowserRouter> 
	</ThemeProvider>
);

ReactDOM.render(<Init />, document.getElementById('root'));