import React, { Component } from 'react';
import Landing from 'Components/Views/Landing';
import scrollToTopOnMount from 'Components/General/scrollToTopOnMount';
import { Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <React.Fragment>
      <Switch>
        <Route exact path="/" component = {scrollToTopOnMount(Landing)}/> 
        <Route render = {()=><h1>Pagina no encontrada en Conkava. Error 404</h1>}/>
      </Switch>
      </React.Fragment>
    );
  }
}

export default App;